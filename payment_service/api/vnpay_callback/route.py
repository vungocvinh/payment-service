from typing import Optional
from fastapi import APIRouter, Depends, HTTPException

from payment_service.core.finish_payment import errors
from payment_service.core.finish_payment.ports import FinishPaymentRequest, FinishPaymentUseCase
from payment_service.libs.vnpay import VNPay

from .dependencies import finish_payment_service, vnpay_client
from .schemas import VNPayCallbackAPIResponse

router = APIRouter()


@router.get("/vnpay", response_model=VNPayCallbackAPIResponse)
def finish_payment(
    vnp_Amount: int,
    vnp_BankCode: str,
    vnp_CardType: str,
    vnp_OrderInfo: str,
    vnp_PayDate: str,
    vnp_ResponseCode: str,
    vnp_TmnCode: str,
    vnp_TransactionNo: str,
    vnp_TransactionStatus: str,
    vnp_TxnRef: str,
    vnp_SecureHash: str,
    vnp_BankTranNo: Optional[str] = None,
    service: FinishPaymentUseCase = Depends(finish_payment_service),
    vnpay_client: VNPay = Depends(vnpay_client),
) -> VNPayCallbackAPIResponse:
    params = {
        "vnp_Amount": vnp_Amount,
        "vnp_BankCode": vnp_BankCode,
        "vnp_CardType": vnp_CardType,
        "vnp_OrderInfo": vnp_OrderInfo,
        "vnp_PayDate": vnp_PayDate,
        "vnp_ResponseCode": vnp_ResponseCode,
        "vnp_TmnCode": vnp_TmnCode,
        "vnp_TransactionNo": vnp_TransactionNo,
        "vnp_TransactionStatus": vnp_TransactionStatus,
        "vnp_TxnRef": vnp_TxnRef,
    }
    if vnp_BankTranNo:
        params["vnp_BankTranNo"] = vnp_BankTranNo

    is_valid = vnpay_client.validate_response(params=params, input_hash=vnp_SecureHash)
    if not is_valid:
        raise HTTPException(status_code=400, detail={"message": "Invalid hash"})
    status = "success" if vnp_ResponseCode == "00" else "failed"
    finish_payment_request = FinishPaymentRequest(payment_id=vnp_TxnRef, status=status)
    try:
        result = service.handle(finish_payment_request)
    except (errors.PaymentNotFound, errors.PaymentAlreadyFinishedError) as e:
        raise HTTPException(status_code=400, detail={"message": str(e)})
    return VNPayCallbackAPIResponse(
        payment_id=result.payment.id,
        payment_amount=result.payment.amount,
        payment_status=result.payment.status,
        balance_amount=result.balance.amount if result.balance else None,
    )
