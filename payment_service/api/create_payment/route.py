from fastapi import APIRouter, Depends, Request

from payment_service.core.create_payment.ports import (
    CreatePaymentUseCase,
    CreatePaymentRequest,
)

from .dependencies import create_payment_service
from .schemas import CreatePaymentBodySchema, CreatePaymentResponseSchema, Payment

router = APIRouter()


@router.post("", response_model=CreatePaymentResponseSchema)
def create_payment(
    request: Request,
    body: CreatePaymentBodySchema,
    service: CreatePaymentUseCase = Depends(create_payment_service),
) -> CreatePaymentResponseSchema:
    client_ip = request.client.host if request.client else "localhost"
    create_payment_request = CreatePaymentRequest(**body.dict(), client_ip=client_ip)
    result = service.create(create_payment_request)
    payment = Payment(**result.payment.dict())
    response = CreatePaymentResponseSchema(
        payment=payment, timestamp=result.timestamp, payment_url=result.payment_url, client_ip=client_ip
    )
    return response
