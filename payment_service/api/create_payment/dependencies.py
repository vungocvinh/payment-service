from fastapi import Depends

from config import create_settings
from payment_service.adapters.create_payment import PostgresPaymentRepository, VNPayBankRepository
from payment_service.core.create_payment import ports
from payment_service.core.create_payment.services import CreatePaymentService
from payment_service.libs.vnpay import VNPay

from ..dependencies import postgres_connection


def create_payment_repository(connection=Depends(postgres_connection)) -> ports.PaymentRepository:
    session = connection()
    return PostgresPaymentRepository(session)


def create_payment_bank_repository(settings=Depends(create_settings)) -> ports.BankRepository:
    return VNPayBankRepository(
        VNPay(
            settings.vnpay.merchant_code,
            settings.vnpay.secret_key,
            settings.vnpay.pay_url,
            settings.vnpay.query_url,
            settings.vnpay.return_url,
        )
    )


def create_payment_service(
    payment_repository: ports.PaymentRepository = Depends(create_payment_repository),
    bank_repository: ports.BankRepository = Depends(create_payment_bank_repository),
) -> ports.CreatePaymentUseCase:
    return CreatePaymentService(payment_repository, bank_repository)
