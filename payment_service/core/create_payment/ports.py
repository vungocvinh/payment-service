from .models import CreatePaymentRequest, CreatePaymentResponse, Payment


class CreatePaymentUseCase:
    def create(self, request: CreatePaymentRequest) -> CreatePaymentResponse:
        raise NotImplementedError()


class PaymentRepository:
    def save(self, payment: Payment) -> None:
        raise NotImplementedError()


class BankRepository:
    def create_payment_url(self, client_ip: str, payment: Payment) -> str:
        raise NotImplementedError()
