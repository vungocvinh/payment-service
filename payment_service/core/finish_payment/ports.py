from typing import Union
from payment_service.core.finish_payment.models import Balance, FinishPaymentRequest, FinishPaymentResponse, Payment


class FinishPaymentUseCase:
    def handle(self, request: FinishPaymentRequest) -> FinishPaymentResponse:
        raise NotImplementedError()


class FinishPaymentRepository:
    def get_payment(self, payment_id: str) -> Union[Payment, None]:
        raise NotImplementedError()

    def finish_payment(self, payment_id: str, status: str) -> Payment:
        raise NotImplementedError()


class BalanceRepository:
    def increase(self, payment: Payment) -> Balance:
        raise NotImplementedError()
