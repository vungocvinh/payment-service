class FinishPaymentError(Exception):
    pass


class PaymentAlreadyFinishedError(FinishPaymentError):
    def __init__(self, payment_id: str, *args: object) -> None:
        message = f"Payment with id {payment_id} already finished"
        super().__init__(message, *args)


class PaymentNotFound(FinishPaymentError):
    def __init__(self, payment_id: str, *args: object) -> None:
        message = f"Payment with id {payment_id} not found"
        super().__init__(message, *args)


class IncreaseBalanceError(FinishPaymentError):
    def __init__(self, payment_id: str, reason: str, *args: object) -> None:
        message = f"Increase balance error for payment with id {payment_id}. Reason: {reason}"
        super().__init__(message, *args)
